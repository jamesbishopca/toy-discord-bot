Discord Bot
===========
Simple bot for Discord written in Python.

Requirements
------------
* discord.py
* sqlalchemy (planned)

Note
----
Since this is a toy project intended for personal use, do not expect much in
the way of documentation or tests.
