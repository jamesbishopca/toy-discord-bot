'''Something will go here.'''


import sqlite3


class Model():
    '''Base class inherited by all models.'''
    db = None # Class attribute shared across instances
    cursor = None # Class attribute shared across instances

    @classmethod
    def init_db(cls, db_path):
        '''Creates the db and cursor objects. Used by all models.'''
        cls.db = sqlite3.connect(db_path)
        cls.cursor = cls.db.cursor()


class YouNow(Model):
    '''Model that associates a YouNow username with a discord user id.'''
    @classmethod
    def create(cls, author_id, younow_user):
        '''Add a new record to the database'''
        cls.cursor.execute('INSERT INTO younow(id, younow_user) VALUES (:id, :younow_user)',
            {'id': author_id, 'younow_user': younow_user})
        cls.db.commit()

    @classmethod
    def read(cls, author_id):
        '''Returns the requested record from the database, or None.'''
        cls.cursor.execute('SELECT younow_user FROM younow WHERE id=?', (author_id,))
        return cls.cursor.fetchone()

    @classmethod
    def update(cls, author_id, username):
        '''Changes an existing record.'''
        cls.cursor.execute('UPDATE younow SET younow_user=? WHERE id=?', (username, author_id))
        cls.db.commit()

    @classmethod
    def delete(cls, author_id):
        '''Deletes the specified record.'''
        cls.cursor.execute('DELETE FROM younow WHERE id=?', (author_id,))
        cls.db.commit()
