DROP TABLE IF EXISTS younow;

CREATE TABLE younow (
  id TEXT PRIMARY KEY,
  younow_user TEXT NOT NULL
);
