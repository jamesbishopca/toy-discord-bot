''' A bot for Discord. Has functionality for Twitch, YouNow, along with
    miscellaneous commands.
    @author     James Bishop
    @license    MIT
    @version    0.1
    @link       https://gitlab.com/jamesbishopca/wintermute_discord_bot
    '''

import asyncio
from datetime import datetime, timedelta
from discord.ext import commands
import json
import models
import os
from random import randint
import re


BASE_PATH = os.path.split(os.path.realpath(__file__))[0]

with open(os.path.join(BASE_PATH, 'config.json'), 'r') as config_file:
    CONFIG = json.loads(config_file.read())


# Create the bot object.
bot = commands.Bot(command_prefix='!', description='I\'m the Matrix.')


# Initialize the database.
models.Model.init_db(CONFIG['dbPath'])


def is_me(m):
    '''Determines if a message is owned by the bot.'''
    return m.author == bot.user


def get_everyone(server, channel):
    '''Takes a server and channel. Returns a mention for @everyone if the bot
       has permission, or the string "everyone" if not.'''
    member = server.get_member(bot.user.id)
    mention_everyone = member.permissions_in(channel).mention_everyone
    return server.default_role if mention_everyone else 'everyone'


def random_response(responses):
    '''Takes a list of responses, and selects one randomly, then returns it.'''
    count = len(responses)
    index = randint(0, count-1)
    return responses[index]


def get_dice(number, die_type):
    '''Rolls dice and returns an array with the results.'''
    return [randint(1, die_type) for _ in range(number)]


def count_hits(results, hit):
    '''Takes an array of results and a target number,
       counts the number of dice that are higher than the target.'''
    count = 0
    for result in results:
        if result >= hit:
            count += 1
    return count


def perform_roll(author, number, die_type, hit, target, explode):
    '''Rolls dice and returns a string with a message for the bot to say.'''
    roll_result = get_dice(number, die_type)
    hits = None
    exploded = 0
    if explode:
        exploded = len([result for result in roll_result if result >= explode])
        roll_result += get_dice(exploded, die_type)
    result_str = ', '.join([str(roll) for roll in roll_result])
    fmt = "{} rolled {}d{}: {}".format(author, number, die_type, result_str)
    fmt += ' ({} exploded)'.format(exploded) if exploded > 0 else ''
    if hit:
        hits = count_hits(roll_result, hit)
        hits_str = " = **{}** hits [hit > {}]" if hits > 1 else " = **{}** hit [hit > {}]"
        fmt += hits_str.format(hits, hit)
    if target:
        if hits is not None:
            total = hits
        else:
            total = sum(roll_result)
            fmt += " = **{}**".format(total)
        success = "***SUCCESS***" if total > target else "***FAILURE***"
        fmt += " against a target of **{}**. {}".format(target, success)
    if not target and not hit:
        fmt += " = **{}**".format(sum(roll_result))
    return fmt


@bot.event
@asyncio.coroutine
def on_ready():
    '''Indicates that the bot is done preparing data received from Discord.'''
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print('------')


@bot.event
@asyncio.coroutine
def on_server_join(server):
    '''Introduces the bot when it joins a new server.'''
    fmt = "Hi, I'm {}. Pleased to meet you. Type `!help` to see what I can do."
    yield from bot.send_message(server.default_channel, fmt.format(bot.user.mention))


@bot.event
@asyncio.coroutine
def on_member_join(member):
    '''Will announce on the main channel when a new member joins.'''
    server = member.server
    if not member.bot:
        fmt = "{} has joined the server. Everyone say hello!"
        yield from bot.send_message(server.default_channel, fmt.format(member.mention))


@bot.event
@asyncio.coroutine
def on_member_update(before, after):
    '''Will announce on the main channel when a stream goes live.'''
    if after.game and after.game.type == 1:
        if before.game and before.game.type == 1:
            return
        else:
            server = after.server
            default_channel = server.default_channel
            everyone = get_everyone(server, default_channel)
            fmt = "Hey {}, {} is live! You can watch here: {}"
            yield from bot.send_message(default_channel, fmt.format(everyone, after.mention, after.game.url))


@bot.command(name='8ball', pass_context=True, description='Get the answers to all of your questions.\nUsage: !8ball Your question here')
@asyncio.coroutine
def magic8ball(ctx):
    '''Returns an answer to the user's question.'''
    author = ctx.message.author
    answers = [
        'It is certain',
        'It is decidedly so',
        'Without a doubt',
        'Yes definitely',
        'You may rely on it',
        'As I see it, yes',
        'Most likely',
        'Outlook good',
        'Yes',
        'Signs point to yes',
        'Reply hazy try again',
        'Ask again later',
        'Better not tell you now',
        'Cannot predict now',
        'Concentrate and ask again',
        'Don\'t count on it',
        'My reply is no',
        'My sources say no',
        'Outlook not so good',
        'Very doubtful'
    ]
    yield from bot.say("{} {}".format(author.mention, random_response(answers)))


@bot.command(pass_context=True, description='Roll some dice.')
@asyncio.coroutine
def roll(ctx, roll_val):
    '''Rolls dice. Takes optional hit target, target number, and number to explode.'''
    author = ctx.message.author
    regex = re.compile('(?P<number>\d+)d(?P<die_type>\d+)(\!(?P<hit>\d+))?(\>(?P<target>\d+))?(\*(?P<explode>\d+))?')
    matched_roll = regex.match(roll_val)
    try:
        number = int(matched_roll.group('number'))
        die_type = int(matched_roll.group('die_type'))
        hit = int(matched_roll.group('hit')) if matched_roll.group('hit') else None
        target = int(matched_roll.group('target')) if matched_roll.group('target') else None
        explode = int(matched_roll.group('explode')) if matched_roll.group('explode') else None
    except (ValueError, AttributeError) as e:
        yield from bot.say("{} Invalid input. Type `!help roll` for more information.")
        return
    if number > 100:
        yield from bot.say('{} Too many dice, please try again. Max: 100'.format(author.mention))
        return
    message = perform_roll(author.mention, number, die_type, hit, target, explode)
    yield from bot.say(message)


@bot.command(pass_context=True, description='Remove messages from the channel')
@asyncio.coroutine
def purge(ctx):
    '''Clears all of the messages in the channel. Mentioning users will clear
       messages for those users only.'''
    channel = ctx.message.channel
    author = ctx.message.author
    targets = ctx.message.mentions
    message_filter = lambda m: m.author in targets
    week_ago = datetime.now() - timedelta(days=7)
    if author.permissions_in(channel).manage_messages:
        if targets:
            deleted = yield from bot.purge_from(channel, limit=10000, check=message_filter, after=week_ago)
        else:
            deleted = yield from bot.purge_from(channel, limit=10000, after=week_ago)
        yield from bot.send_message(channel, 'Deleted {} message(s).'.format(len(deleted)))
    else:
        yield from bot.send_message(channel, 'You don\'t have permission to do that.')


@bot.command(pass_context=True, description='Remove a user from the server.')
@asyncio.coroutine
def kick(ctx):
    '''Removes the mentioned users from the server.'''
    channel = ctx.message.channel
    author = ctx.message.author
    targets = ctx.message.mentions
    if author.permissions_in(channel).kick_members and targets:
        for target in targets:
            yield from bot.kick(target)
            yield from bot.send_message(channel, "{} has been kicked.".format(target.mention))
    else:
        yield from bot.send_message(channel, 'You don\'t have permission to do that.')


@bot.group(name='yn', pass_context=True)
@asyncio.coroutine
def younow(ctx):
    '''Group for YouNow commands. If no subcommand is provided, and user is
       registered, bot will announce that stream is live.'''
    author = ctx.message.author
    yn_acct = models.YouNow.read(author.id)
    everyone = get_everyone(ctx.message.server, ctx.message.channel)
    if ctx.invoked_subcommand is None:
        if yn_acct:
            yn_url = 'https://younow.com/{}'.format(yn_acct[0])
            fmt = "Hey {}, {} is live! You can watch here: {}"
            yield from bot.say(fmt.format(everyone, author.mention, yn_url))
        else:
            yield from bot.say('{} No username on record. Register your username with `!yn register your-username`.'.format(author.mention))


@younow.command(name='register', pass_context=True)
@asyncio.coroutine
def _register(ctx, username):
    '''Takes a YouNow username, and associates it with the Discord user ID.
       Updates username if association exists.'''
    author = ctx.message.author
    yn_acct = models.YouNow.read(author.id)
    if not username:
        yield from bot.say('{} Please enter a username. e.g.: `!yn register your-username`')
        return
    if yn_acct:
        models.YouNow.update(author.id, username)
        yield from bot.say('{} Username updated.'.format(author.mention))
    else:
        models.YouNow.create(author.id, username)
        yield from bot.say('{} Username added.'.format(author.mention))


@younow.command(name='unregister', pass_context=True)
@asyncio.coroutine
def _unregister(ctx):
    '''If username association exists, this command will remove it.'''
    author = ctx.message.author
    yn_acct = models.YouNow.read(author.id)
    if yn_acct:
        models.YouNow.delete(author.id)
        yield from bot.say('{} Username removed.'.format(author.mention))
    else:
        yield from bot.say('{} No username on record.'.format(author.mention))


if __name__ == '__main__':
    bot.run(CONFIG['token'])
